module PFM
  # The InGame skill/move information of a Pokemon
  # @author Nuri Yuri
  class Skill
    # The maximum number of PP the skill has
    # @return [Integer]
    attr_accessor :ppmax
    # The current number of PP the skill has
    # @return [Integer]
    attr_reader :pp
    # If the move has been used
    # @return [Boolean]
    attr_accessor :used
    # The alternative Power information of the skill (dynamic power)
    # @return [Integer, nil]
    attr_accessor :power2
    # The alternatif type information of the skill (dynamic type)
    # @return [Integer, nil] ID of the type
    attr_accessor :type2
    # The alternative accuracy information of the skill (dynamic accuracy)
    # @return [Integer, nil]
    attr_accessor :accuracy2
    # ID of the skill in the Database
    # @return [Integer]
    attr_reader :id
    # Create a new Skill information
    # @param id [Integer] ID of the skill/move in the database
    def initialize(id)
      data = GameData::Skill.get(id)
      @id = id
      unless data
        @ppmax = 0
        @pp = 0
        return
      end
      @ppmax = data.pp_max
      @pp = @ppmax
      @used = false
      # Ivar used when move is changed to another move
      @id_bis = id
      @pp_max_bis = nil
      @pp_bis = nil
      # Ivar used when battle change the move info (dynamic power/type/acc)
      @power2 = nil
      @type2 = nil
      @accuracy2 = nil
    end

    # Reset the skill/move information
    def reset
      @id = @id_bis
      @pp = @pp_bis if @pp_bis
      @ppmax = @pp_max_bis if @pp_max_bis
      @used = false
      @pp_bis = nil
      @pp_max_bis = nil
      @power2 = nil
      @type2 = nil
      @accuracy2 = nil
    end

    # Change the skill information (copy, sketch, Z-move etc...)
    # @param id [Integer] ID of the skill in the database
    # @param pp [Integer, nil] the number of pp of the skill, nil = no change about PPs
    # @param sketch [Boolean] if the skill informations are definitely changed
    def switch(id, pp = 10, sketch = false)
      return initialize(id) if sketch
      data = GameData::Skill.get(id)
      @id_bis = @id
      @pp_bis = @pp if pp
      @pp_max_bis = @ppmax
      @id = id
      unless data
        @pp = @ppmax = 0
        return
      end
      if pp
        pp = data.pp_max if data.pp_max < pp
        @pp = pp
        @ppmax = pp
      end
      @used = false
    end

    # Return the db_symbol of the skill
    # @return [Symbol]
    def db_symbol
      GameData::Skill.db_symbol(@id)
    end

    # Return the name of the skill
    # @return [String]
    def name
      return GameData::Skill.name(@id)
    end

    # Return the symbol of the method to call in BattleEngine
    # @return [Symbol]
    def symbol
      return GameData::Skill.be_method(@id)
    end

    # Return the actual power of the skill
    # @return [Integer]
    def power
      return @power2 || GameData::Skill.power(@id)
    end

    # Return the text of the power of the skill
    # @return [String]
    def power_text
      power = GameData::Skill.power(@id)
      return text_get(11, 12) if power == 0
      return power.to_s
    end

    # Return the text of the PP of the skill
    # @return [String]
    def pp_text
      "#{@pp} / #{@ppmax}"
    end

    # Return the base power (Data power) of the skill
    # @return [Integer]
    def base_power
      return GameData::Skill.power(@id)
    end

    # Return the actual type ID of the skill
    # @return [Integer]
    def type
      return (@type2 || GameData::Skill.type(@id))
    end

    # Return the actual accuracy of the skill
    # @return [Integer]
    def accuracy
      return (@accuracy2 || GameData::Skill.accuracy(@id))
    end

    # Return the accuracy text of the skill
    # @return [String]
    def accuracy_text
      acc = GameData::Skill.accuracy(@id)
      return text_get(11, 12) if acc == 0
      return acc.to_s
    end

    # Return the chance of effect of the skill
    # @return [Integer]
    def effect_chance
      return GameData::Skill.effect_chance(@id)
    end

    # Return the status effect the skill can inflict
    # @return [Integer, nil]
    def status_effect
      return GameData::Skill.status(@id)
    end

    # Return the stat tage modifier the skill can apply
    # @return [Array<Integer>]
    def battle_stage_mod
      return GameData::Skill.battle_stage_mod(@id)
    end

    # Return the target symbol the skill can aim
    # @return [Symbol]
    def target
      return GameData::Skill.target(@id)
    end

    # Is the skill affected by gravity
    # @return [Boolean]
    def gravity_affected?
      return GameData::Skill.gravity(@id)
    end

    # Return the skill description
    # @return [String]
    def description
      return text_get(7, @id) # GameData::Skill.descr(@id)
    end

    # Is the skill direct ?
    # @return [Boolean]
    def direct?
      return GameData::Skill.direct(@id)
    end

    # Is the skill affected by Mirror Move
    # @return [Boolean]
    def mirror_move?
      return GameData::Skill.mirror_move(@id)
    end

    # Return the priority of the skill
    # @return [Integer]
    def priority
      return GameData::Skill.priority(@id)
    end

    # Return the ID of the common event to call on Map use
    # @return [Integer]
    def map_use
      return GameData::Skill.map_use(@id)
    end

    # Is the skill blocable by Protect and skill like that ?
    # @return [Boolean]
    def blocable?
      return GameData::Skill.blocable(@id)
    end

    # Is the skill physical ?
    # @return [Boolean]
    def physical?
      return GameData::Skill.atk_class(@id) == 1
    end

    # Is the skill special ?
    # @return [Boolean]
    def special?
      return GameData::Skill.atk_class(@id) == 2
    end

    # Is the skill status ?
    # @return [Boolean]
    def status?
      return GameData::Skill.atk_class(@id) == 3
    end

    # Return the class of the skill
    # @return [Integer] 1, 2, 3
    def atk_class
      return GameData::Skill.atk_class(@id)
    end

    # Is the skill type normal ?
    # @return [Boolean]
    def type_normal?
      return type == 1
    end

    # Is the skill type fire ?
    # @return [Boolean]
    def type_fire?
      return type == 2
    end
    alias type_feu? type_fire?

    # Is the skill type water ?
    # @return [Boolean]
    def type_water?
      return type == 3
    end
    alias type_eau? type_water?

    # Is the skill type electric ?
    # @return [Boolean]
    def type_electric?
      return type == 4
    end
    alias type_electrique? type_electric?

    # Is the skill type grass ?
    # @return [Boolean]
    def type_grass?
      return type == 5
    end
    alias type_plante? type_grass?

    # Is the skill type ice ?
    # @return [Boolean]
    def type_ice?
      return type == 6
    end
    alias type_glace? type_ice?

    # Is the skill type fighting ?
    # @return [Boolean]
    def type_fighting?
      return type == 7
    end
    alias type_combat? type_fighting?

    # Is the skill type poison ?
    # @return [Boolean]
    def type_poison?
      return type == 8
    end

    # Is the skill type ground ?
    # @return [Boolean]
    def type_ground?
      return type == 9
    end
    alias type_sol? type_ground?

    # Is the skill type fly ?
    # @return [Boolean]
    def type_fly?
      return type == 10
    end
    alias type_vol? type_fly?

    # Is the skill type psy ?
    # @return [Boolean]
    def type_psychic?
      return type == 11
    end
    alias type_psy? type_psychic?

    # Is the skill type insect/bug ?
    # @return [Boolean]
    def type_insect?
      return type == 12
    end

    # Is the skill type rock ?
    # @return [Boolean]
    def type_rock?
      return type == 13
    end
    alias type_roche? type_rock?

    # Is the skill type ghost ?
    # @return [Boolean]
    def type_ghost?
      return type == 14
    end
    alias type_spectre? type_ghost?

    # Is the skill type dragon ?
    # @return [Boolean]
    def type_dragon?
      return type == 15
    end

    # Is the skill type steel ?
    # @return [Boolean]
    def type_steel?
      return type == 16
    end
    alias type_acier? type_steel?

    # Is the skill type dark ?
    # @return [Boolean]
    def type_dark?
      return type == 17
    end
    alias type_tenebre? type_dark?

    # Is the skill type fairy ?
    # @return [Boolean]
    def type_fairy?
      return type == 18
    end
    alias type_fee? type_fairy?

    # Does the skill has recoil ?
    # @return [Boolean]
    def recoil?
      return GameData::Skill.be_method(@id) == :s_recoil
    end

    # Is the skill a punching move ?
    # @return [Boolean]
    def punching?
      return GameData::Skill.punching?(@id)
    end

    # Return the critical rate of the skill
    # @return [Integer]
    def critical_rate
      return GameData::Skill.critical_rate(@id)
    end

    # Is the skill a sound attack ?
    # @return [Boolean]
    def sound_attack?
      return GameData::Skill.sound_attack(@id)
    end

    # Does the skill unfreeze
    # @return [Boolean]
    def unfreeze?
      return GameData::Skill.unfreeze(@id)
    end

    # Does the skill trigger the king rock
    # @return [Boolean]
    def king_rock_utility
      return GameData::Skill.king_rock_utility(@id)
    end

    # Is the skill snatchable ?
    # @return [Boolean]
    def snatchable
      return GameData::Skill.snatchable(@id)
    end

    # Is the skill affected by magic coat ?
    # @return [Boolean]
    def magic_coat_affected
      return GameData::Skill.magic_coat_affected(@id)
    end

    # Change the PP
    # @param v [Integer] the new pp value
    def pp=(v)
      @pp = v
      @pp = @ppmax if @pp > @ppmax
      @pp = 0 if @pp < 0
    end

    # Convert skill to string
    # @return [String]
    def to_s
      return "<S:#{name}_#{power}_#{accuracy}>"
    end

    # List of symbol describe a one target aim
    OneTarget = [:any_other_pokemon, :random_foe, :adjacent_pokemon, :adjacent_foe, :user, :user_or_adjacent_ally, :adjacent_ally]
    # Does the skill aim only one Pokemon
    # @return [Boolean]
    def is_one_target?
      return OneTarget.include?(self.target)
    end

    # List of symbol that doesn't show any choice of target
    TargetNoAsk = [:adjacent_all_foe, :all_foe, :adjacent_all_pokemon, :all_pokemon, :user, :all_ally, :random_foe]
    # Does the skill doesn't show a target choice
    # @return [Boolean]
    def is_no_choice_skill?
      return TargetNoAsk.include?(self.target)
    end
  end
end
